<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>随机测试用户数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchTesterrandomForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td class="title">随机状态:</td>
					<td><select name="A.PSTATE:=" style="width: 120px;">
							<option value=""></option>
							<option value="0">禁用</option>
							<option value="1">启用</option>

					</select></td>
					<td class="title">用户类型</td>
					<td><select name="B.TYPE:=" style="width: 120px;">
							<option value=""></option>
							<option value="1">系统用户</option>
							<option value="2">接口用户</option>
							<option value="3">超级用户</option>
							<option value="4">只读用户/WCP</option>
							<option value="5">知识用户/WCP</option>
							<option value="6">问答用户/WCP</option>
					</select></td>
					<td><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataTesterrandomGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="PSTATE" data-options="sortable:true" width="60">随机状态</th>
					<th field="NAME" data-options="sortable:true" width="60">用户名称</th>
					<th field="LOGINNAME" data-options="sortable:true" width="50">登陆名称</th>
					<th field="TYPE" data-options="sortable:true" width="90">用户类型</th>
					<th field="ORGNAME" data-options="sortable:true" width="90">组织机构</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionTesterrandom = "testerrandom/del.do";//删除URL
	var url_formActionTesterrandom = "testerrandom/form.do";//增加、修改、查看URL
	var url_searchActionTesterrandom = "testerrandom/query.do";//查询URL
	var title_windowTesterrandom = "随机测试用户管理";//功能名称
	var gridTesterrandom;//数据表格对象
	var searchTesterrandom;//条件查询组件对象
	var toolBarTesterrandom = [ {
		id : 'add',
		text : '添加用户',
		iconCls : 'icon-my-account',
		handler : addDataTesterrandom
	}, {
		id : 'edit1',
		text : '启用',
		iconCls : '	icon-eye',
		handler : editStateTesterrandomAble
	}, {
		id : 'edit2',
		text : '禁用',
		iconCls : 'icon-exclamation',
		handler : editStateTesterrandomStop
	}, {
		id : 'del',
		text : '移除用户',
		iconCls : 'icon-remove',
		handler : delDataTesterrandom
	} ];
	$(function() {
		//初始化数据表格
		gridTesterrandom = $('#dataTesterrandomGrid').datagrid({
			url : url_searchActionTesterrandom,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarTesterrandom,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchTesterrandom = $('#searchTesterrandomForm').searchForm({
			gridObj : gridTesterrandom
		});
	});

	//新增
	function addDataTesterrandom() {
		$.farm.openWindow({
			id : 'chooseUserWin',
			width : 600,
			height : 400,
			modal : true,
			url : 'user/chooseUser.do',
			title : '选择用户'
		});
	}
	//添加用户到权限中
	function chooseWindowCallBackHandle(selectedArray) {
		var userids;
		$(selectedArray).each(function(i, obj) {
			if (userids) {
				userids = userids + ',' + obj.ID;
			} else {
				userids = obj.ID;
			}
		});
		$(gridTesterrandom).datagrid('loading');
		//当前组织机构的id
		$.post("testerrandom/add.do", {
			'userids' : userids
		}, function(flag) {
			if (flag.STATE == 0) {
				$('#chooseUserWin').window('close');
				$(gridTesterrandom).datagrid('reload');
			} else {
				$.messager.alert(MESSAGE_PLAT.ERROR, flag.MESSAGE, 'error');
			}
		}, 'json');
	}
	//修改
	function editDataTesterrandom() {
		var selectedArray = $(gridTesterrandom).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionTesterrandom + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winTesterrandom',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	function editStateTesterrandomAble() {
		editStateTesterrandom("1");
	}
	function editStateTesterrandomStop() {
		editStateTesterrandom("0");
	}
	//修改状态
	function editStateTesterrandom(state) {
		var selectedArray = $(gridTesterrandom).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			$(gridTesterrandom).datagrid('loading');
			$.post('testerrandom/editState.do?state=' + state + '&ids='
					+ $.farm.getCheckedIds(gridTesterrandom, 'ID'), {},
					function(flag) {
						var jsonObject = JSON.parse(flag, null);
						$(gridTesterrandom).datagrid('loaded');
						if (jsonObject.STATE == 0) {
							$(gridTesterrandom).datagrid('reload');
						} else {
							var str = MESSAGE_PLAT.ERROR_SUBMIT
									+ jsonObject.MESSAGE;
							$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
						}
					});
		}
	}
	//删除
	function delDataTesterrandom() {
		var selectedArray = $(gridTesterrandom).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridTesterrandom).datagrid('loading');
					$.post(url_delActionTesterrandom + '?ids='
							+ $.farm.getCheckedIds(gridTesterrandom, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridTesterrandom).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridTesterrandom).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>