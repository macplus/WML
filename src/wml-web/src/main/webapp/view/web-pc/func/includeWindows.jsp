<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="modal fade" id="wml_confirm" data-action="none"
	data-type="none" data-success="none" tabindex="-1"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" style="user-select: none;">
		<div class="modal-content"
			style="background-color: #353639; color: #b9bbbd;">
			<div class="modal-header" style="display: none;">
				<h6 class="modal-title"  >确认</h6>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">...</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
				<button type="button" onclick="wmlConfirm_OK()"
					class="btn btn-primary">确认</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function wmlConfirm(msg, actionUrl, type, success_op) {
		if (!actionUrl) {
			alert("參數不能爲空：actionUrl");
		}
		$('#wml_confirm').attr('data-action', actionUrl);
		$('#wml_confirm').attr('data-type', type);
		$('#wml_confirm').attr('data-success', success_op);
		$('.modal-body', '#wml_confirm').text(msg);
		$('.modal-footer', '#wml_confirm').show();
		$('#wml_confirm').modal('show');
	}
	//确认窗口提交事件
	function wmlConfirm_OK() {
		if ($('#wml_confirm').attr('data-type') == 'ajax') {
			$('.modal-footer', '#wml_confirm').hide();
			//ajax
			$
					.post(
							$('#wml_confirm').attr('data-action'),
							{},
							function(flag) {
								if (flag.STATE == 0) {
									//操作成功-刷新文件夹
									if ($('#wml_confirm').attr('data-success') == 'freshFiles') {
										categray_files_refresh();
										$('#wml_confirm').modal('hide');
									}
									//操作成功-提示成功
									if ($('#wml_confirm').attr('data-success') == 'Tip') {
										$('.modal-body', '#wml_confirm').text(
												'操作成功！');
										setTimeout(
												"$('#wml_confirm').modal('hide')",
												"1000");
									}
								} else {
									alert(flag.MESSAGE);
								}
							}, 'json');
		} else {
			//link
			window.open($('#wml_confirm').attr('data-action'));
			$('#wml_confirm').modal('hide')

		}
	}
</script>
<img src="" id="hideImg" style="display: none;" />
<script type="text/javascript">
	function showImg(imgsrc) {
		document.getElementById('hideImg').src = imgsrc;
		var image = new Viewer(document.getElementById('hideImg'), {
			url : 'data-original'
		});
		document.getElementById('hideImg').click();
	}
</script>


<!-- mp4播放器 -->
<div class="modal fade" id="wml_mediaPlay" data-action="none"
	data-type="none" data-success="none" tabindex="-1"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog  modal-lg">
		<div class="modal-content"
			style="background-color: #353639; color: #b9bbbd; user-select: none;">
			<div class="modal-header">
				<h5 class="modal-title" id="wml_mediaPlayLabel"></h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<jsp:include page="includeWindows-mp4.jsp"></jsp:include>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function wmlplayMp4(title, actionUrl) {
		if (!actionUrl) {
			alert("參數不能爲空：actionUrl");
		}
		loadPlayer(actionUrl);
		$('#wml_mediaPlayLabel').text(title);
		$('#wml_mediaPlay').modal('show');
	}
</script>



<!-- mp3播放器 -->
<div class="modal fade" id="wml_musicPlay" data-action="none"
	data-type="none" data-success="none" tabindex="-1"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog  modal-lg">
		<div class="modal-content"
			style="background-color: #353639; color: #b9bbbd; user-select: none;">
			<div class="modal-header">
				<h5 class="modal-title" id="wml_musicPlayLabel"></h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="wml_musicPlayInnerBox"
					style="background: #333333; border-radius: 8px; padding: 20px; padding-bottom: 16px;">
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function wmlplayMp3(title, actionUrl) {
		if (!actionUrl) {
			alert("參數不能爲空：actionUrl");
		}
		$('#wml_musicPlayInnerBox').html(
				'<audio id="wml_musicPlayPlayer" src="' + actionUrl
						+ '" style="width: 100%;" controls preload></audio>');
		$('#wml_musicPlayLabel').text(title);
		$('#wml_musicPlay').modal('show');
	}
</script>
<!-- 目录选择框 -->

<div class="modal fade" id="wml_categrayChoose" data-action="none"
	data-categrayFileid="" data-categrayId="" data-type="none"
	data-success="none" tabindex="-1" aria-labelledby="exampleModalLabel"
	aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content"
			style="background-color: #353639; color: #b9bbbd; user-select: none;">
			<div class="modal-header">
				<h5 class="modal-title">点击选择分类</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="chooseCategrayData"></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#chooseCategrayData').on(
				"select_node.jstree",
				function(e, data) {
					$('#chooseCategrayData').attr('data-categrayId',
							data.selected);
					$.post('opfile/move.do', {
						'categrayFileId' : $('#chooseCategrayData').attr(
								'data-categrayFileid'),
						'categrayId' : $('#chooseCategrayData').attr(
								'data-categrayId')
					}, function(flag) {
						if (flag.STATE == 0) {
							//操作成功
							categray_files_refresh();
						} else {
							alert(flag.MESSAGE);
						}
						$('#wml_categrayChoose').modal('hide');
					}, 'json');
				}).jstree({
			'core' : {
				'data' : {
					"url" : "ui/categrays.do",
					"dataType" : "json" // needed only if you do not supply JSON headers
				}
			}
		});
	});
	//选中分类节点
	function wmlChosseCategray(categrayFileId, categrayId) {
		if (!categrayFileId) {
			alert("參數不能爲空：categrayFileId");
		}
		$('#chooseCategrayData').attr('data-categrayFileid', categrayFileId);
		$('#wml_categrayChoose').modal('show');
		$('#chooseCategrayData').jstree(true).deselect_all();
		$('#chooseCategrayData').jstree(true).open_node(categrayId);
	}
</script>


<!-- iframe框 -->
<div class="modal fade" id="wml_iframewin" data-success="none"
	tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content"
			style="background-color: #353639; color: #b9bbbd; user-select: none;">
			<div class="modal-body">
				<iframe style="width: 100%;" src=""></iframe>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	//选中分类节点
	function wmlOpenIframe(actionUrl) {
		$('iframe', '#wml_iframewin').height($(document).height() * 0.8);
		if (!actionUrl) {
			alert("參數不能爲空：categrayFileId");
		}
		$('iframe', '#wml_iframewin').attr('src', actionUrl);
		$('#wml_iframewin').modal('show');
	}
</script>