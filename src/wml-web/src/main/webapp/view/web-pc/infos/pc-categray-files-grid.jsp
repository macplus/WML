<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<input type="hidden" id="currentCategrayId" value="${categray.id}">
<style>
.wml_ui_file {
	width: 100%;
	text-align: center;
	font-size: 12px;
}

.wml_ui_file .imgbox {
	height: 150px;
}

.wml_ui_file .title {
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
}

.wml_ui_file .tag {
	font-size: 10px;
}

.wml-ui-filsbox .light {
	margin-bottom: 5px;
	padding-top: 5px;
	cursor: pointer;
}

.wml-ui-filsbox .light:hover {
	background-color: #424447;
	border-radius: 5px;
}

.wml-ui-filsbox .light:active {
	background-color: #525457;
	border-radius: 5px;
}

.wml-ui-filsbox .light.active {
	background-color: #424447;
	border-radius: 5px;
}

.wml-ui-filsbox .light.mactive {
	background-color: #666666;
	border-radius: 5px;
}
</style>
<div class="container-fluid wml-ui-filsbox"
	style="margin-bottom: 50px; padding: 34px; user-select: none;">
	<div class="row">
		<c:if test="${col_num<=6||empty files}">
			<c:if test="${!empty parentId&&parentId!='NONE' }">
				<div class="col-${col_num} light ${parentId}"
					onclick="loadWmlCategrayInfo('${parentId}')"
					ondblclick="clickWmlCategray('${parentId}')">
					<div class="wml_ui_file">
						<div class="imgbox">
							<img src="text/img/fileicon/DIR.png" onload="loadedImg(this)"
								style="max-width: 96px; max-height: 96px;">
						</div>
						<div class="title">..</div>
						<div class="tag">返回上级分类</div>
					</div>
				</div>
			</c:if>
			<c:forEach items="${dirs}" var="node">
				<div class="col-${col_num} light ${node.appid}"
					onclick="loadWmlCategrayInfo('${node.appid}')"
					ondblclick="clickWmlCategray('${node.appid}')">
					<div class="wml_ui_file">
						<div class="imgbox">
							<img src="text/img/fileicon/DIR.png" onload="loadedImg(this)"
								style="max-width: 96px; max-height: 96px;">
						</div>
						<div class="title">${node.title }</div>
						<div class="tag" title="文件数量">${node.filenum }/${node.allfilenum }</div>
					</div>
				</div>
			</c:forEach>
		</c:if>
		<c:forEach items="${files}" var="node">
			<div class="col-${col_num} light ${node.appid}" data-fileid="${node.appid}"
				onclick="loadWmlFileInfo('${node.appid}')"
				ondblclick="dblclickFile('${node.title}','${node.id}','${node.modellabel}','${node.exname}')">
				<div class="wml_ui_file">
					<div class="imgbox">
						<c:if test="${fn:indexOf(node.iconUrl, 'fileicon')>0}">
							<img src="${node.iconUrl}" onload="loadedImg(this)"
								style="height: 96px; max-width: 100%; max-height: 100%;">
						</c:if>
						<c:if test="${fn:indexOf(node.iconUrl, 'fileicon')<=0}">
							<img src="${node.iconUrl}" onload="loadedImg(this)"
								style="max-width: 100%; max-height: 100%;">
						</c:if>
					</div>
					<div class="title">${node.title }</div>
					<div class="tag">${node.lenTag }</div>
				</div>
			</div>
		</c:forEach>
	</div>
	<c:if test="${empty files&&empty dirs}">
		<jsp:include page="/view/web-pc/infos/pc-categray-files-none.jsp"></jsp:include>
	</c:if>
</div>
<script type="text/javascript">
	$(function() {
		$(window).resize(function() {
			loadedImg();
			initCenterWinSize();
		});
		initTopButtons('${categrayKey}');
	});
	function loadedImg(img) {
		var imgBoxw = $('.imgbox', '.wml-ui-filsbox').width()
		var imgBoxh = imgBoxw * 0.7;
		$('.imgbox', '.wml-ui-filsbox').height(imgBoxh);
		if (img) {
			var imgH = $(img).height();
			var marginTop = (imgBoxh - imgH) / 2;
			$(img).css('margin-top', marginTop);
			$(img).hide();
			$(img).fadeIn(2000);
		} else {
			$('img', '.wml-ui-filsbox').each(function(i, obj) {
				var imgH = $(obj).height();
				var marginTop = (imgBoxh - imgH) / 2;
				$(obj).css('margin-top', marginTop);
			});
		}
	}
</script>