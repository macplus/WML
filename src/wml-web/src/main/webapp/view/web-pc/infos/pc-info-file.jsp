<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${!empty msg }">
	<h4>错误</h4>
	<table style="width: 100%;">
		<tr>
			<td>${msg}</td>
		</tr>
	</table>
</c:if>
<c:if test="${model=='IMG' }">
	<div style="text-align: center;">
		<img style="max-width: 100%; max-height: 100px; margin-bottom: 4px;"
			alt="" src="download/Pubimg.do?fileid=${img.file.id}&width=500">
	</div>
</c:if>
<c:if test="${model=='MEDIA' }">
	<div style="text-align: center;">
		<i class="bi bi-play-btn" style="font-size: 64px;"></i>
	</div>
</c:if>
<c:if test="${model=='OFFICE' }">
	<div style="text-align: center;">
		<i class="bi bi-file-earmark" style="font-size: 64px;"></i>
	</div>
</c:if>
<c:if test="${!empty colors }">
	<div style="text-align: center;">
		<c:forEach items="${colors}" var="node">
			<div title="${node.colorChName }"
				style="float: left; height: 20px; width: 20px; font-size: 10px; border: 1px solid #ffffff;margin: 4px;background-color:${node.colorHex};">&nbsp;</div>
		</c:forEach>
		<div style="clear: both;"></div>
	</div>
</c:if>
<c:if test="${!empty file }">
	<div style="font-size: 14px;">
		<span class="badge badge-secondary " title="访问量"><i
			class="bi bi-hand-index"></i>&nbsp;${visit.visit}</span> <span title="下载量"
			class="badge badge-success"><i class="bi bi-arrow-down-circle"></i>&nbsp;${visit.downum}</span>
		<span class="badge badge-warning" title="预览量"><i
			class="bi bi-eye"></i>&nbsp;${visit.viewnum}</span>
	</div>
	<div style="text-align: center;">
		<div class="btn-toolbar" role="toolbar"
			style="margin: auto; margin-top: 8px; text-align: center;"
			aria-label="Toolbar with button groups">
			<div class="btn-group mr-2 btn-group-sm" role="group"
				aria-label="First group">
				<!-- IMG, MEDIA, OFFICE, FILE -->
				<c:if test="${model=='IMG' }">
					<a type="button" class="btn btn-secondary"
						onclick="showImg('download/Pubimg.do?fileid=${file.file.id}')">查看</a>
				</c:if>
				<c:if test="${file.file.exname=='mp4'}">
					<a type="button" class="btn btn-secondary"
						onclick="wmlplayMp4('${file.file.title}', 'download/Pubload.do?id=${file.file.id}&secret=${file.file.secret}')">播放</a>
				</c:if>
				<c:if test="${file.file.exname=='mp3'}">
					<a type="button" class="btn btn-secondary"
						onclick="wmlplayMp3('${file.file.title}', 'download/Pubload.do?id=${file.file.id}&secret=${file.file.secret}')">播放</a>
				</c:if>
				<c:if test="${file.file.exname=='pdf'}">
					<a type="button" class="btn btn-secondary"
						onclick="wmlOpenIframe('download/Pubload.do?id=${file.file.id}')">预览</a>
				</c:if>
				<!--  -->
				<a type="button" class="btn btn-secondary"
					onclick="wmlConfirm('立即下载文件<${file.file.title}>吗?','download/Pubfile.do?id=${file.file.id}&secret=${file.file.secret}','link')">下载</a>

				<c:if test="${empty pstate||pstate=='1' }">
					<c:if test="${empty visitmode }">
						<a type="button" class="btn btn-secondary"
							onclick="wmlChosseCategray('${categrayFileId}','${categray.id}')">移动</a>

						<a type="button" class="btn btn-secondary"
							onclick="wmlConfirm('确认收藏文件<${file.file.title}>吗?','opfile/favorites.do?categrayFileId=${categrayFileId}','ajax','Tip')">收藏</a>
						<!--  -->
						<a type="button" class="btn btn-secondary"
							onclick="wmlConfirm('确认删除文件<${file.file.title}>吗?','opfile/del.do?categrayFileId=${categrayFileId}','ajax','freshFiles')">删除</a>
					</c:if>
					<c:if test="${!empty visitmode&& visitmode=='FAVORITES'}">
						<a type="button" class="btn btn-secondary"
							onclick="wmlConfirm('确认取消收藏文件<${file.file.title}>吗?','opfile/delvlog.do?vlogid=${vlogid}','ajax','freshFiles')">取消收藏</a>
					</c:if>
					<c:if test="${!empty visitmode&& visitmode=='VISIT'}">
						<a type="button" class="btn btn-secondary"
							onclick="wmlConfirm('确认删除文件<${file.file.title}>访问记录吗?','opfile/delvlog.do?vlogid=${vlogid}','ajax','freshFiles')">删除访问记录</a>
					</c:if>
				</c:if>
				<c:if test="${!empty pstate&&pstate=='0' }">
					<a type="button" class="btn btn-secondary"
						onclick="wmlConfirm('确认永久删除文件<${file.file.title}>吗?','opfile/phdel.do?categrayFileId=${categrayFileId}','ajax','freshFiles')">永久删除</a>
					<a type="button" class="btn btn-secondary"
						onclick="wmlConfirm('确认还原文件<${file.file.title}>吗?','opfile/redel.do?categrayFileId=${categrayFileId}','ajax','freshFiles')">还原</a>
				</c:if>
			</div>
		</div>
	</div>
	<h4>文件信息</h4>
	<table style="width: 100%;">
		<tr>
			<td style="width: 100px;" title="${model}">名称:</td>
			<td style="word-break: break-all; word-wrap: break-word;">${file.file.title}</td>
		</tr>
		<tr>
			<td style="width: 100px;">大小:</td>
			<td>${file.sizelabel}</td>
		</tr>
		<tr>
			<td style="width: 100px;">格式:</td>
			<td>${file.file.exname}</td>
		</tr>
		<c:if test="${!empty categray }">

			<tr>
				<td style="width: 100px;">分类名称:</td>
				<td>${categray.name}</td>
			</tr>

		</c:if>
	</table>
</c:if>
<c:if test="${!empty tags }">
	<div class="wml-ui-tags">
		<c:forEach items="${tags }" var="node">
			<div class="tag">${node.name }</div>
		</c:forEach>
	</div>
	<div style="clear: both;"></div>
</c:if>
<c:if test="${!empty visit}">
	<jsp:include page="pc-info-file-praise.jsp"></jsp:include>
</c:if>
<c:if test="${!empty visit}">
	<jsp:include page="pc-info-file-comments.jsp"></jsp:include>
</c:if>