<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
.wml-comments-msg {
	border-top: 2px dashed #444548;
	margin-top: 10px;
	padding-top: 20px;
}

.wml-comments-msg img {
	width: 32px;
	height: 32px;
	border-radius: 16px;
}

.wml-comments-msg a {
	text-decoration: underline;
	color: #ffc107;
}

.wml-comments-msg a:hover {
	text-decoration: underline;
	color: #ffffff;
}
</style>
<div style="margin-top: 20px;">
	<div class="input-group input-group-sm">
		<textarea class="form-control" aria-label="With textarea"
			id="wmlCommentInput"
			onkeypress="if (event.keyCode == 13) {submitConmments();}"
			style="background-color: #424447; border: #424447; color: #ffffff;"></textarea>
		<div class="input-group-prepend">
			<button class="btn btn-outline-secondary" type="button"
				onclick="submitConmments() ;" id="button-addon1">
				发表<br />评论
			</button>
		</div>
	</div>
	<div id="wmlCommentsBox">
		<c:forEach items="${comments }" var="node" varStatus="status">
			<c:if test="${status.index==0 }">
				<div class="media wml-comments-msg">
					<img src="download/PubPhoto.do?id=${node.cuser }" class="mr-3">
					<div class="media-body">
						<p>
							<span style="color: #666666;"> ${node.cusername }:<PF:FormatTime
									date="${node.ctime}" yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
							</span> <br />${node.strflag }
						</p>
					</div>
				</div>
			</c:if>
		</c:forEach>
	</div>
	<div id="wmlAllComments" style="display: none;">
		<c:forEach items="${comments }" var="node" varStatus="status">
			<c:if test="${status.index>0 }">
				<div class="media wml-comments-msg">
					<img src="download/PubPhoto.do?id=${node.cuser }" class="mr-3">
					<div class="media-body">
						<p>
							<span style="color: #666666;"> ${node.cusername }:<PF:FormatTime
									date="${node.ctime}" yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
							</span> <br />${node.strflag }
						</p>
					</div>
				</div>
			</c:if>
		</c:forEach>
	</div>
	<c:if test="${fn:length(comments)>1}">
		<div style="text-align: right;">
			<a href="javascript:void(0);"
				onclick="$('#wmlAllComments').show();$(this).hide();">展示<span
				style="color: #ffc107;">剩余${fn:length(comments)-1}条</span>评论内容
			</a>
		</div>
	</c:if>
</div>
<script type="text/javascript">
	function submitConmments() {
		var msg = $('#wmlCommentInput').val().trim();
		if (msg) {
			$
					.post(
							'opfile/comments.do',
							{
								'appId' : '${empty file.file.id?categray.id:file.file.id}',
								'msg' : $('#wmlCommentInput').val().trim()
							},
							function(flag) {
								if (flag.STATE == 0) {
									var html = '<div class="media wml-comments-msg" id="'+flag.data.id+'"><img src="download/PubPhoto.do?id='
											+ flag.data.cuser
											+ '" class="mr-3" ><div class="media-body"><p><b>刚刚</b>&nbsp;&nbsp;'
											+ flag.data.strflag
											+ '&nbsp;&nbsp;<a href="javascript:void(0);" onclick="delCommentsCurrent(\''
											+ flag.data.id
											+ '\')" >删除</a></p></div></div>';
									$('#wmlCommentsBox').prepend(html);
									$('#wmlCommentInput').val('')
								} else {
									alert(flag.MESSAGE);
								}
							}, 'json');
		}
	}

	function delCommentsCurrent(logid) {
		$.post('opfile/delcomments.do', {
			'logid' : logid
		}, function(flag) {
			if (flag.STATE == 0) {
				$('#' + logid).remove();
			} else {
				alert(flag.MESSAGE);
			}
		}, 'json');
	}
</script>