<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div style="padding: 10px;user-select: none;">
	<table>
		<tr>
			<td><img class="img-rounded"
				src="<PF:basePath/>webfile/Publogo.do" height="32" alt="WLP"
				align="middle" /></td>
			<td><div
					style="font-size: 20px; padding-left: 10px; color: #ffffff;">
					<PF:ParameterValue key="config.sys.title" />
				</div></td>
		</tr>
	</table>
</div>
