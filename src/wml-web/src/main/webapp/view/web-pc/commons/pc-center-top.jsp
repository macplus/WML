<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="container-fluid"
	style="overflow: hidden; height: 46px; user-select: none;">
	<div class="row">
		<div class="col-lg-5" style="padding: 15px; padding-left: 30px;">
			<div class="menus-node" style="float: left;">
				<a onclick="openTopMenuWin_column()" style="width: 200px;"><i
					class="bi bi-layout-three-columns"></i>&nbsp;列数 </a>
			</div>
			<div class="menus-node" style="float: left;">
				<a onclick="openTopMenuWin_layout()" style="width: 200px;"><i
					class="bi bi-columns"></i>&nbsp;布局 </a>
			</div>
			<div class="menus-node" style="float: left;">
				<a onclick="openTopMenuWin_sort()" style="width: 200px;"> <i
					class="bi bi-sort-alpha-down"></i>&nbsp;排序
				</a>
			</div>
			<div class="menus-node" style="float: left;"
				id="wmlTopButtonSelectId">
				<a onclick="openTopMenuWin_select()" style="width: 200px;"><i
					class="bi bi-files"></i>&nbsp;多选 </a>
			</div>
			<div class="menus-node" style="float: left;">
				<a onclick="categray_files_refresh()" style="width: 200px;"><i
					class="bi bi-repeat"></i>&nbsp;刷新 </a>
			</div>
		</div>
		<div class="col-lg-3 d-none d-lg-block" style="padding: 15px;">
			<div class="menus-node" id="wmlTopButtonUploadId">
				<a onclick="openTopMenuWin_upload()" style="width: 200px;">&nbsp;&nbsp;&nbsp;
					<i class="bi bi-plus-square"></i>&nbsp;添加资源&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</a>
			</div>
		</div>
		<div class="col-lg-4 d-none d-lg-block ">
			<div style="padding: 10px; padding-top: 12px;"
				class="wml-ui-dir-search">
				<div class="input-group input-group-sm mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text" id="inputGroup-sizing-sm"><i
							class="bi bi-search"></i></span>
					</div>
					<input type="text" class="form-control" placeholder="查找素材"
						id="wml_ui_fileSearch_id" aria-label="Sizing example input"
						aria-describedby="inputGroup-sizing-sm">
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var currentfiles_url = null;
	function openTopMenuWin_upload() {
		if (topmenu_win_type == 'UPLOAD' && topmenu_win_stat == 'OPEN') {
			closeTopMenuWin();
		} else {
			$('.wml_ui_plusWin').hide();
			$('#wml_ui_plusWin_upload').show();
			topmenu_win_type = 'UPLOAD';
			openTopMenuWin();
		}
	}
	function openTopMenuWin_column() {
		if (topmenu_win_type == 'COLUMN' && topmenu_win_stat == 'OPEN') {
			closeTopMenuWin();
		} else {
			$('.wml_ui_plusWin').hide();
			$('#wml_ui_plusWin_column').show();
			topmenu_win_type = 'COLUMN';
			openTopMenuWin();
		}
	}
	function openTopMenuWin_layout() {
		if (topmenu_win_type == 'LAYOUT' && topmenu_win_stat == 'OPEN') {
			closeTopMenuWin();
		} else {
			$('.wml_ui_plusWin').hide();
			$('#wml_ui_plusWin_layout').show();
			topmenu_win_type = 'LAYOUT';
			openTopMenuWin();
		}
	}
	function openTopMenuWin_sort() {
		if (topmenu_win_type == 'SORT' && topmenu_win_stat == 'OPEN') {
			closeTopMenuWin();
		} else {
			$('.wml_ui_plusWin').hide();
			$('#wml_ui_plusWin_sort').show();
			topmenu_win_type = 'SORT';
			openTopMenuWin();
		}
	}
	
	function openTopMenuWin_select() {
		if (topmenu_win_type == 'SELECT' && topmenu_win_stat == 'OPEN') {
			closeTopMenuWin();
			topmenu_win_type = 'NONE';
			$('.wml-ui-filsbox .light').removeClass('mactive');
		} else {
			$('.wml_ui_plusWin').hide();
			$('#wml_ui_plusWin_select').show();
			topmenu_win_type = 'SELECT';
			openTopMenuWin();
		}
	}
	
	
	function categray_files_refresh() {
		topmenu_win_type = 'NONE';
		closeTopMenuWin();
		loadFiles();
		loadWmlCategrayInfo($('#currentCategrayId').val());
	}
	function loadFiles(url) {
		if (url) {
			currentfiles_url = url;
		}
		if (currentfiles_url) {
			var colum = $("input[name='wml_input_colum']:checked").val();
			var layout = $("input[name='wml_input_layout']:checked").val();
			var sort = $("input[name='wml_input_sort']:checked").val();
			var paras = "colum=" + colum + "&layout=" + layout + "&sort="
					+ sort;
			$('#wml_ui_categray_files').load(
					currentfiles_url.indexOf('?') >= 0 ? currentfiles_url + "&"
							+ paras : currentfiles_url + "?" + paras, {
						'word' : $('#wml_ui_fileSearch_id').val().trim()
					});
		}
	}

	$(function() {
		//选择列事件
		$("input[name='wml_input_colum']").click(function() {
			loadFiles();
		});
		//选择排序事件
		$("input[name='wml_input_sort']").click(function() {
			loadFiles();
		});
		//选择布局事件
		$("input[name='wml_input_layout']").click(function() {
			loadFiles();
		});
		//檢索框查询事件
		$('#wml_ui_fileSearch_id').bind('keypress', function(event) {
			if (event.keyCode == "13") {
				loadFiles();
			}
		});
	});
</script>