<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<base href="<PF:basePath/>">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1,user-scalable=no" />
<link rel="icon" href="favicon.ico" mce_href="favicon.ico"
	type="image/x-icon">
<link rel="shortcut icon" href="favicon.ico" mce_href="favicon.ico"
	type="image/x-icon">
<script type="text/javascript" src="text/javascript/jquery1113.js"></script>
<script type="text/javascript"
	src="text/lib/layout/jquery.layout-latest.js"></script>
<link rel="stylesheet" href="text/lib/bootstrap4/css/bootstrap.min.css">
<link rel="stylesheet" href="text/lib/bootstrap4/css/icon.css">
<script src="text/lib/bootstrap4/js/bootstrap.bundle.min.js" ></script>

<link rel="stylesheet" href="text/lib/viewer/viewer.css">
<script src="text/lib/viewer/viewer.js" ></script>

<script type="text/javascript">
	var basePath = '<PF:basePath/>';
	$(function() {
		$.ajaxSetup({
			cache : false
		});
	})
//-->
</script>