<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>事件模型数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<script src="text/lib/echarts/echarts.min.4.4.0.js"></script>
<script src="text/lib/d3Cloud/d3.min.js"></script>
<script src="text/lib/d3Cloud/d3.layout.cloud.js"></script>
</head>
<body>
	<div id="d3_cloud" style="width: 100px;height: 200px;"></div>
	<script>
	var fill = d3.scale.category20();  //输出20种类别的颜色 ---颜色比例尺
	var words_list = [
        {text:"互联网医疗", size:'20'},
        {text:"基因检测", size:'30'},
        {text:"医疗服务", size:'26'},
        {text:"再生医学", size:'55'},
        {text:"生物科技", size:'26'},
        {text:"医药", size:'34'},
        {text:"免疫治疗", size:'16'},
        {text:"体外诊断", size:'20'},
        {text:"医疗设备", size:'30'},
        {text:"医疗影像", size:'24'},
        {text:"脑科学", size:'20'},
    ];
	var layout = d3.layout.cloud()
    .size([300, 500])
    .words(words_list)
    .padding(5)
    .rotate(function() { return ~~(Math.random() * 2) * 90; })
    .font("Impact")
    .fontSize(function(d) { return d.size; })
    .on("end", draw);

layout.start();

function draw(words) {
  	d3.select("#d3_cloud").append("svg")
      .attr("width", layout.size()[0])
      .attr("height", layout.size()[1])
      .append("g")
      .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
      .selectAll("text")
      .data(words)
      .enter().append("text")
      .style("font-size", function(d) { return d.size + "px"; })
      .style("font-family", "Impact")
      .attr("text-anchor", "middle").style("fill", function(d, i) { return fill(i); })
      .attr("transform", function(d) {
        return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
      })
      .text(function(d) { return d.text; });
}
 </script>
</body>
</html>