<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>通讯方式设置- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="index,follow">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
<script src="text/javascript/base64.js"></script>
<script src="text/javascript/md5.js"></script>
<script src="text/javascript/encode.provider.js"></script>
<script src="text/javascript/jssha256.js"></script>
<style>
.wlp-v-list ul li {
	list-style-type: none;
	padding: 10px;
	color: #666666;
	padding-top: 20px;
	padding-bottom: 20px;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<!-- 导航 -->
			<jsp:include page="../commons/head.jsp"></jsp:include>
		</div>
	</div>
	<div class="container-fluid"
		style="padding-top: 20px; padding-bottom: 20px;">
		<div class="container" style="margin-top: 50px;">
			<div class="row">
				<div class="col-md-3">
					<jsp:include page="commons/include-setting-leftmenu.jsp"><jsp:param
							name="current" value="contact" /></jsp:include>
				</div>
				<div class="col-md-9">
					<div style="margin-top: 20px;">
						<div class="wlp-userspace-h2">
							<div class="row">
								<div class="col-xs-6">通讯方式设置</div>
								<div class="col-xs-6">
									<div class="input-group input-group-sm"></div>
								</div>
							</div>
						</div>
						<hr class="wlp-userspace-hr">
						<div style="padding: 20px;">
							<div class="row">
								<div class="col-mx-6">
									<form role="form"
										action="userspace/editCurrentUserPwdCommit.do"
										id="registSubmitFormId" method="post">
										<c:forEach items="${types }" var="node">
											<c:if test="${node.key!='USERKEY' }">
												<div class="row" style="margin: 20px;">
													<div class="col-md-12">

														<div class="input-group" id="${node.key}GroupId">
															<span class="input-group-addon" id="sizing-addon1">${node.title }:</span>
															<input type="text" class="form-control"
																value="${addreses[node.key].addr }"
																id="${node.key}DomId" placeholder="${node.title }">
															<span class="input-group-btn">
																<button id="${node.key}buttonId" class="btn btn-success"
																	type="button">保存</button>
															</span>
														</div>

														<!-- /input-group -->
														<p class="bg-danger"
															style="color: #a94442; background-color: #ffffff; font-size: 12px; margin-top: 4px;"
															id="${node.key}errorBoxId"></p>
													</div>
												</div>
												<script type="text/javascript">
													$(function() {

														$(
																'#${node.key}buttonId')
																.bind(
																		'click',
																		function() {
																			if (!validate('${node.key}GroupId')) {
																				$('#${node.key}errorBoxId')
																						.show();
																			} else {
																				$('#${node.key}errorBoxId')
																						.hide();
																				$.post('userspace/savecontact.do',
																								{'key':'${node.key}','addr':$('#${node.key}DomId').val()},
																								function(flag){
																									if(flag.STATE=='0'){
																										alert("保存成功!");
																									}else{
																										alert(flag.MESSAGE);
																									}
																								},'json')
																			}
																		});

														validateInput(
																'${node.key}DomId',
																function(id,
																		val,
																		obj) {
																	// 当前密码
																	//if (valid_isNull(val)) {
																	//	return {
																	//		valid : false,
																	//		msg : '不能为空'
																	//	};
																	//}
																	if ('${node.key}' == 'EMAIL') {
																		if (!valid_isNull(val)
																				&& !isEmail(val)) {
																			return {
																				valid : false,
																				msg : '邮件地址格式错误!'
																			};
																		}
																	}
																	if ('${node.key}' == 'MPHONE') {
																		if (!valid_isNull(val)
																				&& !isPhone(val)) {
																			return {
																				valid : false,
																				msg : '移动电话格式错误!'
																			};
																		}
																	}
																	if (valid_maxLength(
																			val,
																			256)) {
																		return {
																			valid : false,
																			msg : '不能大于256个字符'
																		};
																	}
																	return {
																		valid : true,
																		msg : '正确'
																	};
																},
																'${node.key}errorBoxId',
																'${node.key}GroupId');
													});
												</script>
											</c:if>
										</c:forEach>
										<div class="row">
											<div class="col-md-12" style="padding-left: 50px;">
												<button type="button"  onclick="gotoBackUrl()"
												class="btn btn-default wucBackButton">返回</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid" style="padding: 0px;">
		<!-- 页脚 -->
		<jsp:include page="../commons/foot.jsp"></jsp:include>
	</div>
</body>
</html>