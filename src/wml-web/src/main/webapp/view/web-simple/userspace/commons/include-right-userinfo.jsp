<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<c:if test="${USEROBJ.imgid}">
	<div style="width: 100%; background-color: #f8fafc; margin-top: 20px;">
		<div style="padding: 20px; text-align: center;" class="hidden-xs">
			<!-- 头像用户名 -->
			<div>
				<img alt="" src="download/PubPhoto.do?id=${USEROBJ.imgid}"
					style="width: 96px; height: 96px;" class="img-circle">
			</div>
			<div style="font-size: 16px; margin-top: 8px;">${USEROBJ.name}</div>
		</div>
	</div>
</c:if>