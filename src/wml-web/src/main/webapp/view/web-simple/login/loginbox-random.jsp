<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<form class="form-signin" role="form" id="loginFormId"
	action="login/randomerLogin.do" method="post">
	<input type="hidden" name="sysid" value="${sysid}">
	<div class="form-group">
		<label for="exampleInputEmail1"> 用户类型 </label> <select
			class="form-control" style="margin-top: 4px;" id="loginNameId"
			name="type" required autofocus>
			<PF:OptionDictionary index="USER_RANDOM_LOGINTYPE"
				isTextValue="false" />
		</select>
	</div>
	<div class="form-group" id="loginCheckFormId">
		<label for="exampleInputEmail1">验证码</label>
		<div class="input-group">
			<input type="text" class="form-control" placeholder="请录入验证码"
				id="checkcodeId" name="checkcode">
			<div class="input-group-addon" style="padding: 0px;">
				<img id="checkcodeimgId"
					style="cursor: pointer; height: 30px; width: 100px;"
					src="webfile/Pubcheckcode.do" />
			</div>
		</div>
	</div>
	<!-- <input type="hidden" name="url" id="loginUrlId"> -->
	<div>
		<button class="btn btn-danger text-left" id="loginButtonId"
			style="margin-top: 4px; width: 100%;" type="button">
			<i class="glyphicon glyphicon-random"
				style="position: relative; top: 3px;"></i>&nbsp;&nbsp;登录随机用户
		</button>
	</div>
</form>
<c:if test="${STATE=='1'}">
	<div class="text-center" id="romovealertMessageErrorId"
		style="margin: 4px; color: red; border-top: 1px dashed #ccc; padding-top: 20px;">
		<span class="glyphicon glyphicon-exclamation-sign"></span> ${MESSAGE}
	</div>
</c:if>
<div class="text-center" id="alertMessageErrorId"
	style="margin: 4px; color: red; border-top: 1px dashed #ccc; padding-top: 20px;"></div>
<script type="text/javascript">
	$(function() {
		$('#alertMessageErrorId').hide();
		$('#loginButtonId').bind('click', function() {
			doSubmitLoginForm();
		});
		$('#checkcodeimgId').bind(
				"click",
				function(e) {
					$('#checkcodeimgId').attr(
							"src",
							"webfile/Pubcheckcode.do?time="
									+ new Date().getTime());
				});
		$('#checkcodeId').keydown(function(e) {
			if (e.keyCode == 13) {
				//$('#loginUrlId').val(window.location.href);
				$('#loginButtonId').click();
			}
		});
	});

	//执行登陆
	function doSubmitLoginForm() {
		//校验验证码
		if (!$('#checkcodeId').val()) {
			$('#alertMessageErrorId').show();
			$('#romovealertMessageErrorId').hide();
			$('#alertMessageErrorId')
					.html(
							'<span class="glyphicon glyphicon-exclamation-sign"></span>请输入验证码');
			return;
		}
		$('#loginButtonId').addClass("disabled");
		$('#loginButtonId').text("提交中...");
		$('#loginFormId').submit();
	}
</script>