<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<PF:basePath/>">
    <title>文件统计数据管理</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <jsp:include page="/view/conf/include.jsp"></jsp:include>
  </head>
  <body class="easyui-layout">
    <div data-options="region:'north',border:false">
      <form id="searchFilevisitForm">
        <table class="editTable">
          <tr style="text-align: center;">
            <td colspan="4">
              <a id="a_search" href="javascript:void(0)"
                class="easyui-linkbutton" iconCls="icon-search">查询</a>
              <a id="a_reset" href="javascript:void(0)"
                class="easyui-linkbutton" iconCls="icon-reload">清除条件</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div data-options="region:'center',border:false">
      <table id="dataFilevisitGrid">
        <thead>
          <tr>
            <th data-options="field:'ck',checkbox:true"></th>
            <th field="FILEID" data-options="sortable:true" width="60">FILEID</th>
            <th field="VIEWNUM" data-options="sortable:true" width="30">预览量</th>
            <th field="DOWNUM" data-options="sortable:true" width="30">下载量</th>
            <th field="ID" data-options="sortable:true" width="20">ID</th>
          </tr>
        </thead>
      </table>
    </div>
  </body>
  <script type="text/javascript">
    var url_delActionFilevisit = "filevisit/del.do";//删除URL
    var url_formActionFilevisit = "filevisit/form.do";//增加、修改、查看URL
    var url_searchActionFilevisit = "filevisit/query.do";//查询URL
    var title_windowFilevisit = "文件统计管理";//功能名称
    var gridFilevisit;//数据表格对象
    var searchFilevisit;//条件查询组件对象
    var toolBarFilevisit = [ {
      id : 'view',
      text : '查看',
      iconCls : 'icon-tip',
      handler : viewDataFilevisit
    }, {
      id : 'add',
      text : '新增',
      iconCls : 'icon-add',
      handler : addDataFilevisit
    }, {
      id : 'edit',
      text : '修改',
      iconCls : 'icon-edit',
      handler : editDataFilevisit
    }, {
      id : 'del',
      text : '删除',
      iconCls : 'icon-remove',
      handler : delDataFilevisit
    } ];
    $(function() {
      //初始化数据表格
      gridFilevisit = $('#dataFilevisitGrid').datagrid( {
        url : url_searchActionFilevisit,
        fit : true,
        fitColumns : true,
        'toolbar' : toolBarFilevisit,
        pagination : true,
        closable : true,
        checkOnSelect : true,
        border:false,
        striped : true,
        rownumbers : true,
        ctrlSelect : true
      });
      //初始化条件查询
      searchFilevisit = $('#searchFilevisitForm').searchForm( {
        gridObj : gridFilevisit
      });
    });
    //查看
    function viewDataFilevisit() {
      var selectedArray = $(gridFilevisit).datagrid('getSelections');
      if (selectedArray.length == 1) {
        var url = url_formActionFilevisit + '?pageset.pageType='+PAGETYPE.VIEW+'&ids='
            + selectedArray[0].;
        $.farm.openWindow( {
          id : 'winFilevisit',
          width : 600,
          height : 300,
          modal : true,
          url : url,
          title : '浏览'
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
            'info');
      }
    }
    //新增
    function addDataFilevisit() {
      var url = url_formActionFilevisit + '?operateType='+PAGETYPE.ADD;
      $.farm.openWindow( {
        id : 'winFilevisit',
        width : 600,
        height : 300,
        modal : true,
        url : url,
        title : '新增'
      });
    }
    //修改
    function editDataFilevisit() {
      var selectedArray = $(gridFilevisit).datagrid('getSelections');
      if (selectedArray.length == 1) {
        var url = url_formActionFilevisit + '?operateType='+PAGETYPE.EDIT+ '&ids=' + selectedArray[0].;
        $.farm.openWindow( {
          id : 'winFilevisit',
          width : 600,
          height : 300,
          modal : true,
          url : url,
          title : '修改'
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
            'info');
      }
    }
    //删除
    function delDataFilevisit() {
      var selectedArray = $(gridFilevisit).datagrid('getSelections');
      if (selectedArray.length > 0) {
        // 有数据执行操作
        var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
        $.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
          if (flag) {
            $(gridFilevisit).datagrid('loading');
            $.post(url_delActionFilevisit + '?ids=' + $.farm.getCheckedIds(gridFilevisit,''), {},
              function(flag) {
                var jsonObject = JSON.parse(flag, null);
                $(gridFilevisit).datagrid('loaded');
                if (jsonObject.STATE == 0) {
                  $(gridFilevisit).datagrid('reload');
              } else {
                var str = MESSAGE_PLAT.ERROR_SUBMIT
                    + jsonObject.MESSAGE;
                $.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
              }
            });
          }
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
            'info');
      }
    }
  </script>
</html>