<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--素材分类表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formCategray">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">上級分类:</td>
					<td><c:if test="${empty parent}">无</c:if>${parent.name}<input
						type="hidden" name="parentid" value="${parent.id}"></td>
					<td class="title"><c:if test="${pageset.operateType==0}">ID:</c:if></td>
					<td><c:if test="${pageset.operateType==0}">${entity.id}</c:if>
					</td>
				</tr>
				<tr>
					<td class="title">名称:</td>
					<td><input type="text" style="width: 130px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[512]']"
						id="entity_name" name="name" value="${entity.name}"></td>
					<td class="title">排序:</td>
					<td><input type="text" style="width: 130px;"
						class="easyui-validatebox"
						data-options="required:true,validType:['integer','maxLength[5]']"
						id="entity_sort" name="sort" value="${entity.sort}"></td>
				</tr>
				<c:if test="${pageset.operateType==1}">
					<tr>
						<td class="title"></td>
						<td>多个分类可用逗号分隔</td>
						<td class="title"></td>
						<td></td>
					</tr>
				</c:if>
				<tr>
					<td class="title">状态</td>
					<td><select style="width: 130px;" class="easyui-validatebox"
						data-options="required:true" id="entity_pstate" name="pstate"
						val="${entity.pstate}">
							<option value="1">可用</option>
							<option value="0">禁用</option>
					</select></td>
					<td class="title"></td>
					<td></td>
				</tr>
				<tr>
					<td class="title">备注:</td>
					<td colspan="3"><textarea type="text" style="width: 420px;"
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[256]']" id="entity_pcontent"
							name="pcontent">${entity.pcontent}</textarea></td>
				</tr>
				<!-- <tr>
					<td class="title">内容排序方式:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_csort" name="csort" value="${entity.csort}"></td>
				</tr>
				<tr>
					<td class="title">内容布局方式:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_clayout" name="clayout" value="${entity.clayout}">
					</td>
				</tr>
				<tr>
					<td class="title">内容模型:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_cmodel" name="cmodel" value="${entity.cmodel}">
					</td>
				</tr> -->
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityCategray" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityCategray" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formCategray" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionCategray = 'categray/add.do';
	var submitEditActionCategray = 'categray/edit.do';
	var currentPageTypeCategray = '${pageset.operateType}';
	var submitFormCategray;
	$(function() {
		//表单组件对象
		submitFormCategray = $('#dom_formCategray').SubmitForm({
			pageType : currentPageTypeCategray,
			grid : gridCategray,
			currentWindowId : 'winCategray'
		});
		//关闭窗口
		$('#dom_cancle_formCategray').bind('click', function() {
			$('#winCategray').window('close');
		});
		//提交新增数据
		$('#dom_add_entityCategray').bind('click', function() {
			submitFormCategray.postSubmit(submitAddActionCategray, function() {
				$.messager.confirm('确认对话框', '数据更新,是否重新加载左侧树？', function(r) {
					if (r) {
						$('#categrayTree').tree('reload');
					}
				});
				$('#winCategray').window('close');
			});
		});
		//提交修改数据
		$('#dom_edit_entityCategray').bind('click', function() {
			submitFormCategray.postSubmit(submitEditActionCategray, function() {
				$.messager.confirm('确认对话框', '数据更新,是否重新加载左侧树？', function(r) {
					if (r) {
						$('#categrayTree').tree('reload');
					}
				});
				$('#winCategray').window('close');
			});
		});
	});
//-->
</script>