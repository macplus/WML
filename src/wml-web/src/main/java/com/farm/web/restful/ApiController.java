package com.farm.web.restful;

import java.io.File;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.farm.authority.FarmAuthorityService;
import com.farm.authority.domain.Organization;
import com.farm.authority.domain.User;
import com.farm.authority.password.PasswordProviderService;
import com.farm.authority.service.OrganizationServiceInter;
import com.farm.authority.service.UserServiceInter;
import com.farm.core.auth.domain.LoginUser;
import com.farm.core.auth.exception.LoginUserNoAuditException;
import com.farm.core.auth.exception.LoginUserNoExistException;
import com.farm.core.page.ViewMode;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.file.FarmFileServiceInter;
import com.farm.file.domain.FileBase;
import com.farm.file.enums.FileModel;
import com.farm.file.exception.FileExNameException;
import com.farm.file.util.FileCopyProcessCache;
import com.farm.parameter.FarmParameterService;
import com.farm.parameter.service.ParameterServiceInter;
import com.farm.util.cache.FarmCacheGenerater;
import com.farm.util.cache.FarmCacheName;
import com.farm.util.cache.FarmCaches;
import com.farm.util.web.IpComparaUtils;
import com.farm.web.WebUtils;
import com.farm.web.domain.Results;
import com.farm.web.filter.utils.LoginCertificateUtils;

/**
 * 组织机构、 [创建、查询、更新、删除] 用户、 [创建、查询、更新、删除] ---------------------------- 知识接口[查询]、
 * 分类接口[查询]、 问答接口[查询]、
 * 
 * @author wangdong
 *
 */
@RequestMapping("/api")
@Controller
public class ApiController extends WebUtils {
	private final static Logger log = Logger.getLogger(ApiController.class);
	@Resource
	private OrganizationServiceInter organizationServiceImpl;
	@Resource
	private UserServiceInter userServiceImpl;
	@Resource
	private FarmFileServiceInter farmFileServiceImpl;
	@Resource
	private ParameterServiceInter parameterServiceImpl;

	/**
	 * 验证秘钥和用户信息
	 * 
	 * @param secret
	 */
	public static void checkAuth(final String loginname, final String plaintextPassword) {
		if (FarmParameterService.getInstance().getParameter("config.restful.debug").equals("true")) {
			return;
		}
		if (FarmParameterService.getInstance().getParameter("config.restful.secret.type").equals("complex")) {
			if (!(Boolean) FarmCaches.getInstance().getCacheData(loginname + "-" + plaintextPassword,
					new FarmCacheGenerater() {
						@Override
						public Object generateData() {
							try {
								checkUser(loginname, plaintextPassword);
							} catch (Exception e) {
								return false;
							}
							return true;
						}
					}, FarmCacheName.farm_api_usercheck)) {
				checkUser(loginname, plaintextPassword);
			}
		}
	}

	/**
	 * 如果校验错误会抛出异常
	 * 
	 * @param loginname
	 * @param plaintextPassword
	 */
	private static void checkUser(final String loginname, final String plaintextPassword) {

		try {
			if (StringUtils.isBlank(loginname) || StringUtils.isBlank(plaintextPassword)) {
				throw new RuntimeException("loginname or password is null!");
			}
			if (!FarmAuthorityService.getInstance().isLegalityByInterUser(loginname,
					PasswordProviderService.getInstanceProvider().getClientPassword(loginname, plaintextPassword))) {
				throw new RuntimeException("user authentication is wrong! by verification error!");
			}
			if (!FarmAuthorityService.getInstance().getUserByLoginName(loginname).getType().equals("9")) {
				throw new RuntimeException("user is not interface user!");
			}
		} catch (LoginUserNoExistException e) {
			throw new RuntimeException("user authentication is wrong! by LoginUserNoExistException!");
		} catch (LoginUserNoAuditException e) {
			throw new RuntimeException("user authentication is wrong! by LoginUserNoAuditException!");
		}

	}

	/**
	 * 验证秘钥
	 * 
	 * @param secret    用户接口密钥
	 * @param sysSecret 子系统密钥(为空时验证系统密钥)
	 * @param request
	 */
	public static void checkSecret(String secret, HttpServletRequest request) {
		String ip = getCurrentIp(request);
		boolean whiteListAble = FarmParameterService.getInstance()
				.getParameterBoolean("config.restful.whitelist.state");
		if (whiteListAble) {
			String ips = FarmParameterService.getInstance().getParameter("config.restful.whitelist.ips");
			if (StringUtils.isNotBlank(ips)) {
				List<String> ipWhiteList = WebUtils.parseIds(ips.replaceAll("，", ","));
				if (StringUtils.isBlank(ip) || !(IpComparaUtils.isContainIp(ipWhiteList, ip))) {
					log.warn("WHITE-LIST:" + IpComparaUtils.getWhiteListTitle(ipWhiteList));
					throw new RuntimeException("the ip is not exist white list:" + ip);
				}
			} else {
				throw new RuntimeException("the ip white list is blank!");
			}
		}
		String state = FarmParameterService.getInstance().getParameter("config.restful.state");
		if (!state.toLowerCase().equals("true")) {
			throw new RuntimeException("the api is unable,check config file please!");
		}
		if (secret == null) {
			if (FarmParameterService.getInstance().getParameter("config.restful.debug").equals("true")) {
				return;
			} else {
				throw new RuntimeException("secret not exist!");
			}
		}
		if (!FarmParameterService.getInstance().getParameter("config.restful.secret.type").equals("none")) {
			log.info("have received remoteSecret:" + secret);
			// 从配置文件中读取秘钥
			String sysKey = FarmParameterService.getInstance().getParameter("config.restful.secret.key").trim();
			if (!secret.trim().equals(sysKey)) {
				if (!FarmParameterService.getInstance().getParameter("config.restful.debug").equals("true")) {
					throw new RuntimeException("secret error!");
				}
			}
		}
	}

	/**
	 * 获得当前用户
	 * 
	 * @return
	 */
	private LoginUser getUser() {
		return new LoginUser() {
			@Override
			public String getName() {
				return "restful";
			}

			@Override
			public String getLoginname() {
				return "restful";
			}

			@Override
			public String getId() {
				return "restful";
			}

			@Override
			public String getType() {
				return "NONE";
			}

			@Override
			public String getIp() {
				return "NONE";
			}
		};
	}

	/**
	 * 获得当前用户
	 * 
	 * @return
	 */
	private LoginUser getUser(String loginname) {
		if (StringUtils.isBlank(loginname)) {
			return getUser();
		}
		return FarmAuthorityService.getInstance().getUserByLoginName(loginname);
	}

	/**
	 * 查询所有组织机构
	 * 
	 * @param ID
	 * @param NAME
	 * @param PARENTID
	 * @param APPID
	 * @param session
	 * @return
	 */
	@RequestMapping("/get/organization")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> getOrganization(String id, String name, String parentid, String appid,
			String secret, String operatorLoginname, String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:查询组织机构");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			DataQuery query = DataQuery.getInstance();
			// --------------------------------------------
			// ------------------------------------------
			if (StringUtils.isNotBlank(id)) {
				query.addRule(new DBRule("id", id, "="));
			}
			if (StringUtils.isNotBlank(name)) {
				query.addRule(new DBRule("name", name, "="));
			}
			if (StringUtils.isNotBlank(parentid)) {
				query.addRule(new DBRule("parentid", parentid, "="));
			}
			if (StringUtils.isNotBlank(appid)) {
				query.addRule(new DBRule("appid", appid, "="));
			}
			query.setPagesize(10000);
			// query.addRule(new DBRule("state", "1", "="));
			DataQuery dbQuery = DataQuery.init(query, "ALONE_AUTH_ORGANIZATION",
					"ID,TYPE,SORT,PARENTID,MUSER,CUSER,STATE,UTIME,CTIME,COMMENTS,NAME,TREECODE,APPID");
			DataResult result = dbQuery.search();
			// ------------------------------------------
			// ------------------------------------------
			Results resultObj = Results.getResults(result.getResultList(), result.getTotalSize(),
					result.getCurrentPage(), result.getPageSize());
			return ViewMode.getInstance().putAttr("DATA", resultObj).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 获得UTC时间
	 *
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping(value = "/secret/utc")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> utc(HttpSession session) {
		log.info("restful API:获得UTC时间，用于制作权限码");
		try {
			Calendar cal = Calendar.getInstance();
			return ViewMode.getInstance().putAttr("UTC", cal.getTimeInMillis()).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 创建组织机构
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/post/organization")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> postOrganization(String parentid, String sort, String name,
			String comments, String secret, String appid, String operatorLoginname, String operatorPassword,
			HttpServletRequest request) {
		try {
			log.info("restful API:创建组织机构");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			Organization entity = new Organization();
			if (StringUtils.isNotBlank(parentid)) {
				entity.setParentid(parentid);
			}
			entity.setSort(Integer.valueOf(sort));
			entity.setName(name);
			entity.setComments(comments);
			entity.setType("1");
			entity.setAppid(appid);
			entity = organizationServiceImpl.insertOrganizationEntity(entity, getUser(operatorLoginname));
			return ViewMode.getInstance().putAttr("ID", entity.getId()).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 更新组织机构
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/put/organization")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> putOrganization(String id, String sort, String name, String comments,
			String parentid, String secret, String operatorLoginname, String operatorPassword,
			HttpServletRequest request) {
		try {
			log.info("restful API:修改组织机构");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			Organization entity = organizationServiceImpl.getOrganizationEntity(id);
			if (StringUtils.isNotBlank(sort)) {
				entity.setSort(Integer.valueOf(sort));
			}
			if (StringUtils.isNotBlank(name)) {
				entity.setName(name);
			}
			if (StringUtils.isNotBlank(comments)) {
				entity.setComments(comments);
			}
			entity = organizationServiceImpl.editOrganizationEntity(entity, getUser(operatorLoginname));
			if (StringUtils.isNotBlank(parentid)) {
				organizationServiceImpl.moveOrgTreeNode(entity.getId(), parentid, getUser(operatorLoginname));
			}
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 删除组织机构
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/delete/organization")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> delOrganization(String id, String secret, String operatorLoginname,
			String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:删除组织机构");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			organizationServiceImpl.deleteOrganizationEntity(id, getUser(operatorLoginname));
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 查询所有用户
	 * 
	 * @param ID
	 * @param NAME
	 * @param PARENTID
	 * @param APPID
	 * @param session
	 * @return
	 */
	@RequestMapping("/get/user")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> getUser(String id, String loginname, String type, String state,
			String orgid, String secret, String operatorLoginname, String operatorPassword,
			HttpServletRequest request) {
		try {
			log.info("restful API:查询用户");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			DataQuery query = DataQuery.getInstance();
			// --------------------------------------------
			// ------------------------------------------
			if (StringUtils.isNotBlank(id)) {
				query.addRule(new DBRule("USER.ID", id, "="));
			}
			if (StringUtils.isNotBlank(loginname)) {
				query.addRule(new DBRule("USER.LOGINNAME", loginname, "="));
			}
			if (StringUtils.isNotBlank(type)) {
				query.addRule(new DBRule("USER.TYPE", type, "="));
			}
			if (StringUtils.isNotBlank(state)) {
				query.addRule(new DBRule("USER.STATE", state, "="));
			}
			if (StringUtils.isNotBlank(orgid)) {
				query.addRule(new DBRule("RFORG.ORGANIZATIONID", orgid, "="));
			}
			query.setPagesize(10000);
			DataQuery dbQuery = DataQuery.init(query,
					"ALONE_AUTH_USER USER left join ALONE_AUTH_USERORG RFORG on USER.ID=RFORG.USERID",
					"USER.ID as ID,USER.NAME as NAME,RFORG.ORGANIZATIONID as ORGANIZATIONID,USER.COMMENTS as COMMENTS,USER.TYPE as TYPE,USER.LOGINNAME as LOGINNAME,USER.IMGID as IMGID,USER.STATE as STATE");
			DataResult result = dbQuery.search();
			// ------------------------------------------
			// ------------------------------------------
			Results resultObj = Results.getResults(result.getResultList(), result.getTotalSize(),
					result.getCurrentPage(), result.getPageSize());
			return ViewMode.getInstance().putAttr("DATA", resultObj).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 创建用户
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/post/user")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> postUser(String name, String loginname, String state, String type,
			String comments, String orgid, String secret, String operatorLoginname, String operatorPassword,
			HttpServletRequest request) {
		try {
			log.info("restful API:创建用户");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			User entity = new User();
			entity.setLoginname(loginname);
			entity.setName(name);
			if (StringUtils.isNotBlank(comments)) {
				entity.setComments(comments);
			}
			if (StringUtils.isNotBlank(type)) {
				entity.setType(type);
			} else {
				entity.setType("1");
			}
			if (StringUtils.isNotBlank(state)) {
				entity.setState(state);
			} else {
				entity.setState("1");
			}
			entity = userServiceImpl.insertUserEntity(entity, getUser(operatorLoginname));
			if (StringUtils.isNotBlank(orgid)) {
				userServiceImpl.setUserOrganization(entity.getId(), orgid, getUser());
			}
			return ViewMode.getInstance().putAttr("ID", entity.getId()).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 更新用户
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/put/user")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> putUser(String id, String name, String loginname, String state,
			String comments, String orgid, String type, String secret, String operatorLoginname,
			String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:修改用户");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			User entity = userServiceImpl.getUserEntity(id);
			if (StringUtils.isNotBlank(name)) {
				entity.setName(name);
			}
			if (StringUtils.isNotBlank(loginname)) {
				entity.setLoginname(loginname);
			}
			if (StringUtils.isNotBlank(comments)) {
				entity.setComments(comments);
			}
			if (StringUtils.isNotBlank(type)) {
				entity.setType(type);
			}
			if (StringUtils.isNotBlank(state)) {
				entity.setState(state.equals("0") ? "0" : "1");
			}
			entity = userServiceImpl.editUserEntity(entity, getUser(operatorLoginname));
			if (StringUtils.isNotBlank(orgid)) {
				userServiceImpl.setUserOrganization(id, orgid, getUser(operatorLoginname));
			}
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 删除用户
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/delete/user")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> delUser(String id, String secret, String operatorLoginname,
			String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:删除用户");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			userServiceImpl.deleteUserEntity(id, getUser(operatorLoginname));
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 用户登录注册,返回后的验证码可以用来在前台登录
	 * 
	 * @param loginname
	 * @param secret
	 * @param session
	 * @return
	 */
	@RequestMapping("/regist/login")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> registLogin(String loginname, String secret, String operatorLoginname,
			String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:查询用户");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			// ------------------------------------------
			// 判断登录名是否存在
			if (userServiceImpl.getUserByLoginName(loginname) == null) {
				throw new RuntimeException("loginname is not exist!");
			}
			// 通过用登录名注册到
			String uuid = "";
			if (FarmParameterService.getInstance().getParameterBoolean("config.login.noauth.able")) {
				uuid = LoginCertificateUtils.registLoginCertificate(loginname);
			} else {
				uuid = "notAble";
			}
			// ------------------------------------------
			return ViewMode.getInstance().putAttr("CERTIFICATE", uuid).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 通过用户登录注册的验证码,查找用户的登录名
	 * 
	 * @param loginname
	 * @param secret
	 * @param session
	 * @return
	 */
	@RequestMapping("/get/login")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> getLogin(String certificate, String secret, HttpServletRequest request) {
		try {
			log.info("restful API:查询用户");
			checkSecret(secret, request);
			// ------------------------------------------
			String loginname = LoginCertificateUtils.getLoginNameByCertificate(certificate);
			// ------------------------------------------
			return ViewMode.getInstance().putAttr("LOGINNAME", loginname).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 查询系統參數
	 * 
	 * @param key
	 * @param paras             附加参数，特殊情况下会需要带入参数（获取头像：用户登陆名，）
	 * @param secret
	 * @param operatorLoginname
	 * @param operatorPassword
	 * @param request
	 * @return
	 */
	@RequestMapping("/get/parameter")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> getParameter(String key, String paras, String secret,
			String operatorLoginname, String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:查询系統參數");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			String val = FarmParameterService.getInstance().getParameter(key);
			return ViewMode.getInstance().putAttr("DATA", val).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 设置系統參數
	 * 
	 * @param ID
	 * @param NAME
	 * @param PARENTID
	 * @param APPID
	 * @param session
	 * @return
	 */
	@RequestMapping("/put/parameter")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> putParameter(String key, String val, String secret,
			String operatorLoginname, String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:查询系統參數");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			parameterServiceImpl.setValue(key, val, getUser(operatorLoginname));
			parameterServiceImpl.refreshCache();
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}
}
