package com.wml.client.domain;

public class ClientUser {

	private String id;
	private String name;
	private String loginname;
	private String type;
	private String ip;

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLoginname() {
		return loginname;
	}

	public String getType() {
		return type;
	}

	public String getIp() {
		return ip;
	}

}
