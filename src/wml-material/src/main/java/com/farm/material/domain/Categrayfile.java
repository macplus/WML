package com.farm.material.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "CategrayFile")
@Table(name = "wml_c_categrayfile")
public class Categrayfile implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "CATEGRAYID", length = 32, nullable = false)
        private String categrayid;
        @Column(name = "FILEID", length = 32, nullable = false)
        private String fileid;
        @Column(name = "PSTATE", length = 2, nullable = false)
        private String pstate;
        @Column(name = "DUSER", length = 32)
        private String duser;
        
        
        public String getDuser() {
			return duser;
		}
		public void setDuser(String duser) {
			this.duser = duser;
		}
		public String getPstate() {
			return pstate;
		}
		public void setPstate(String pstate) {
			this.pstate = pstate;
		}
		public String  getCategrayid() {
          return this.categrayid;
        }
        public void setCategrayid(String categrayid) {
          this.categrayid = categrayid;
        }
        public String  getFileid() {
          return this.fileid;
        }
        public void setFileid(String fileid) {
          this.fileid = fileid;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}