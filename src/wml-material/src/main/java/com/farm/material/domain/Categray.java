package com.farm.material.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：素材分类类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "Categray")
@Table(name = "wml_c_categray")
public class Categray implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "PCONTENT", length = 512)
        private String pcontent;
        @Column(name = "PSTATE", length = 2, nullable = false)
        private String pstate;
        @Column(name = "EUSER", length = 32, nullable = false)
        private String euser;
        @Column(name = "CUSER", length = 32, nullable = false)
        private String cuser;
        @Column(name = "ETIME", length = 16, nullable = false)
        private String etime;
        @Column(name = "CTIME", length = 16, nullable = false)
        private String ctime;
        @Column(name = "SORT", length = 10, nullable = false)
        private Integer sort;
        @Column(name = "PARENTID", length = 32, nullable = false)
        private String parentid;
        @Column(name = "NAME", length = 32, nullable = false)
        private String name;
        @Column(name = "TREECODE", length = 256, nullable = false)
        private String treecode;
        @Column(name = "TYPE", length = 2, nullable = false)
        private String type;
        @Column(name = "CSORT", length = 32, nullable = false)
        private String csort;
        @Column(name = "CLAYOUT", length = 32, nullable = false)
        private String clayout;
        @Column(name = "CMODEL", length = 32, nullable = false)
        private String cmodel;
        @Column(name = "CNUM", length = 10, nullable = false)
        private Integer cnum;
        @Column(name = "ALLNUM", length = 10, nullable = false)
        private Integer allnum;

        public String  getPcontent() {
          return this.pcontent;
        }
        public void setPcontent(String pcontent) {
          this.pcontent = pcontent;
        }
        public String  getPstate() {
          return this.pstate;
        }
        public void setPstate(String pstate) {
          this.pstate = pstate;
        }
        public String  getEuser() {
          return this.euser;
        }
        public void setEuser(String euser) {
          this.euser = euser;
        }
        public String  getCuser() {
          return this.cuser;
        }
        public void setCuser(String cuser) {
          this.cuser = cuser;
        }
        public String  getEtime() {
          return this.etime;
        }
        public void setEtime(String etime) {
          this.etime = etime;
        }
        public String  getCtime() {
          return this.ctime;
        }
        public void setCtime(String ctime) {
          this.ctime = ctime;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
        public Integer  getSort() {
          return this.sort;
        }
        public void setSort(Integer sort) {
          this.sort = sort;
        }
        public String  getParentid() {
          return this.parentid;
        }
        public void setParentid(String parentid) {
          this.parentid = parentid;
        }
        public String  getName() {
          return this.name;
        }
        public void setName(String name) {
          this.name = name;
        }
        public String  getTreecode() {
          return this.treecode;
        }
        public void setTreecode(String treecode) {
          this.treecode = treecode;
        }
        public String  getType() {
          return this.type;
        }
        public void setType(String type) {
          this.type = type;
        }
        public String  getCsort() {
          return this.csort;
        }
        public void setCsort(String csort) {
          this.csort = csort;
        }
        public String  getClayout() {
          return this.clayout;
        }
        public void setClayout(String clayout) {
          this.clayout = clayout;
        }
        public String  getCmodel() {
          return this.cmodel;
        }
        public void setCmodel(String cmodel) {
          this.cmodel = cmodel;
        }
        public Integer  getCnum() {
          return this.cnum;
        }
        public void setCnum(Integer cnum) {
          this.cnum = cnum;
        }
        public Integer  getAllnum() {
          return this.allnum;
        }
        public void setAllnum(Integer allnum) {
          this.allnum = allnum;
        }
}