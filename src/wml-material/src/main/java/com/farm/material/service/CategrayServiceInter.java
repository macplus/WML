package com.farm.material.service;

import com.farm.material.domain.Categray;
import com.farm.material.domain.ex.JsTreeNode;
import com.farm.core.sql.query.DataQuery;
import com.farm.file.domain.ex.FileView;
import com.farm.file.service.VisitServiceInter.OpModel;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;


public interface CategrayServiceInter {
	
	public Categray insertCategrayEntity(Categray entity, LoginUser user);

	
	public Categray editCategrayEntity(Categray entity, LoginUser user);

	
	public void deleteCategrayEntity(String id, LoginUser user);

	
	public Categray getCategrayEntity(String id);

	
	public DataQuery createCategraySimpleQuery(DataQuery query);

	public void moveTreeNode(String ids, String targetId, LoginUser currentUser);

	
	public List<JsTreeNode> getAllCategrays(String name, LoginUser currentUser);

	
	public void bindFile(String categrayid, String fileid);

	
	public List<FileView> getFiles(String categrayid, Integer page);

	
	public List<FileView> getCategrays(String categrayid);

	
	public List<FileView> getVlogFiles(LoginUser currentUser, OpModel model, Integer page);

	
	public List<FileView> getVlogCategrays(LoginUser currentUser, OpModel model, Integer page);

	
	public List<FileView> getRecycleFiles(LoginUser currentUser, Integer page);
}