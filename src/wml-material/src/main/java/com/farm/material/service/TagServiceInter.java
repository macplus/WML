package com.farm.material.service;

import com.farm.material.domain.Tag;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;


public interface TagServiceInter {
	
	public Tag insertTagEntity(Tag entity, LoginUser user);

	
	public Tag editTagEntity(Tag entity, LoginUser user);

	
	public void deleteTagEntity(String id, LoginUser user);

	
	public Tag getTagEntity(String id);

	
	public DataQuery createTagSimpleQuery(DataQuery query);

	
	public List<Tag> getTags(String categrayId);
}