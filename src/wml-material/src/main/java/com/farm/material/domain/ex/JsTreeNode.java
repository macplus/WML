package com.farm.material.domain.ex;

import java.io.Serializable;
import java.util.List;


public class JsTreeNode implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private String text;
	private String parentid;
	private List<JsTreeNode> children;

	public JsTreeNode(String id, String text,int num) {
		super();
		this.id = id;
		this.text = text;
	}

	public JsTreeNode(String id,String parentid, String text ,int num) {
		super();
		this.id = id;
		this.parentid = parentid;
		
		if(num>0) {
			text=text+"<i>"+num+"</i>";
		}
		
		
		
		this.text = text;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<JsTreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<JsTreeNode> children) {
		this.children = children;
	}

}
