package com.farm.file.service;

import com.farm.file.domain.Visitlogs;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.auth.domain.LoginUser;

public interface VisitlogsServiceInter{
  
  public Visitlogs insertVisitlogsEntity(Visitlogs entity,LoginUser user);
  
  public Visitlogs editVisitlogsEntity(Visitlogs entity,LoginUser user);
  
  public void deleteVisitlogsEntity(String id,LoginUser user);
  
  public Visitlogs getVisitlogsEntity(String id);
  
  public DataQuery createVisitlogsSimpleQuery(DataQuery query);
}