package com.farm.file.util;

import com.farm.util.cache.FarmCacheName;
import com.farm.util.cache.FarmCaches;


public class FileCopyProcessCache {

	public static void setProcess(String key, Integer percentage) {
		FarmCaches.getInstance().putCacheData(key, percentage, FarmCacheName.FileProcess);
	}

	public static Integer getProcess(String key) {
		Object process = FarmCaches.getInstance().getCacheData(key, FarmCacheName.FileProcess);
		if (process != null) {
			return (Integer) process;
		} else {
			return 0;
		}
	}

}
