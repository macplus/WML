package com.farm.file.exception;


public class FileExNameException extends Exception {

	
	private static final long serialVersionUID = -2169402402793297968L;

	public FileExNameException(String message) {
		super(message);
	}

}
