package com.farm.file.dao;

import com.farm.file.domain.FileText;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;



public interface FileTextDaoInter  {
 
 public void deleteEntity(FileText filetext) ;
 
 public FileText getEntity(String filetextid) ;
 
 public  FileText insertEntity(FileText filetext);
 
 public int getAllListNum();
 
 public void editEntity(FileText filetext);
 
 public Session getSession();
 
 public DataResult runSqlQuery(DataQuery query);
 
 public void deleteEntitys(List<DBRule> rules);

 
 public List<FileText> selectEntitys(List<DBRule> rules);

 
 public void updataEntitys(Map<String, Object> values, List<DBRule> rules);
 
 public int countEntitys(List<DBRule> rules);
}