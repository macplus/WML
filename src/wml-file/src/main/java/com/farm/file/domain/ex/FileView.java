package com.farm.file.domain.ex;

import com.farm.core.time.TimeTool;
import com.farm.file.domain.FileBase;
import com.farm.file.domain.FileResource;
import com.farm.file.domain.Visit;
import com.farm.file.enums.FileModel;
import com.farm.util.web.FarmFormatUnits;


public class FileView {
	// 文件对象
	private FileBase file;

	private String title;

	private String lenTag;

	private String id;

	private String appid;

	private String exname;

	private String ctime;

	private int len;

	// 下载地址
	private String downloadUrl;
	// 图标地址
	private String iconUrl;
	// 预览地址
	private String viewUrl;
	// 状态标签
	private String statelabel;
	// 模型标签
	private String modellabel;
	// 资源库
	private FileResource resource;
	// 真实地址
	private String realpath;
	
	private boolean physicsExist;
	// 文件大小标签
	private String sizelabel;
	// 统计对象
	private Visit visit;
	private Integer viewNum;
	private Integer dowNum;

	// 文件夾時的屬性，記錄内容數量
	private int filenum;
	private int allfilenum;

	public FileView() {
		super();
	}

	public FileView(String title, int len, String fileid, String appid, String exname, String ctime) {
		super();
		this.title = title;
		this.lenTag = FarmFormatUnits.getFileLengthAndUnit(len);
		this.len = len;
		this.id = fileid;
		this.exname = exname;
		this.modellabel = FileModel.getModelByFileExName(exname).name();
		this.appid = appid;
		this.ctime = TimeTool.getFormatTimeDate12(ctime, "yyyy-MM-dd HH:mm");
		if (FileModel.getModelByFileExName(exname).equals(FileModel.IMG)) {
			this.iconUrl = "download/Pubimg.do?fileid=" + fileid + "&width=500";
		} else {
			this.iconUrl = "text/img/fileicon/" + exname.toUpperCase() + ".png";
		}
	}

	public FileView(String title, String appid, String exname, String ctime, int num, int allnum) {
		super();
		this.title = title;
		this.lenTag = FarmFormatUnits.getFileLengthAndUnit(len);
		this.exname = exname;
		this.modellabel = FileModel.getModelByFileExName(exname).name();
		this.appid = appid;
		this.ctime = TimeTool.getFormatTimeDate12(ctime, "yyyy-MM-dd HH:mm");
		this.filenum = num;
		this.allfilenum = allnum;
	}

	public int getFilenum() {
		return filenum;
	}

	public void setFilenum(int filenum) {
		this.filenum = filenum;
	}

	public int getAllfilenum() {
		return allfilenum;
	}

	public void setAllfilenum(int allfilenum) {
		this.allfilenum = allfilenum;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public int getLen() {
		return len;
	}

	public void setLen(int len) {
		this.len = len;
	}

	public String getCtime() {
		return ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLenTag() {
		return lenTag;
	}

	public void setLenTag(String lenTag) {
		this.lenTag = lenTag;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getExname() {
		return exname;
	}

	public void setExname(String exname) {
		this.exname = exname;
	}

	public Integer getDowNum() {
		return dowNum;
	}

	public void setDowNum(Integer dowNum) {
		this.dowNum = dowNum;
	}

	public Integer getViewNum() {
		return viewNum;
	}

	public String getSizelabel() {
		return sizelabel;
	}

	public boolean isPhysicsExist() {
		return physicsExist;
	}

	public void setPhysicsExist(boolean physicsExist) {
		this.physicsExist = physicsExist;
	}

	public void setSizelabel(String sizelabel) {
		this.sizelabel = sizelabel;
	}

	public String getRealpath() {
		return realpath;
	}

	public void setRealpath(String realpath) {
		this.realpath = realpath;
	}

	public FileResource getResource() {
		return resource;
	}

	public void setResource(FileResource resource) {
		this.resource = resource;
	}

	public String getStatelabel() {
		return statelabel;
	}

	public void setStatelabel(String statelabel) {
		this.statelabel = statelabel;
	}

	public String getModellabel() {
		return modellabel;
	}

	public void setModellabel(String modellabel) {
		this.modellabel = modellabel;
	}

	public FileBase getFile() {
		return file;
	}

	public void setFile(FileBase file) {
		this.file = file;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getViewUrl() {
		return viewUrl;
	}

	public Visit getVisit() {
		return visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public void setViewUrl(String viewUrl) {
		this.viewUrl = viewUrl;
	}

	public void setViewNum(Integer viewnum) {
		this.viewNum = viewnum;
	}

	public void setDownum(Integer downum) {
		this.dowNum = downum;
	}
}
