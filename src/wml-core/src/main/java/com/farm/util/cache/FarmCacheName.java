package com.farm.util.cache;


public enum FarmCacheName {
	// 附件缓存
	farm_file_persist("farm_file_persist"),
	// 记录用户登陆失败，用于屏蔽登陆
	farm_login_fail("farm_login_fail"),
	// 文件處理進度緩存
	FileProcess("wml-file-process"),
	// 接口调用时接口用户校验结果的缓存
	farm_api_usercheck("farm_api_usercheck");

	
	private String permanentCacheName;

	FarmCacheName(String permanentCacheName) {
		this.permanentCacheName = permanentCacheName;
	}

	
	public String getPermanentCacheName() {
		return permanentCacheName;
	}
}
