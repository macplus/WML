package com.farm.wcp.api.util;

public class Http404Exception extends RuntimeException {
	public Http404Exception(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
