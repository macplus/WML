# WML素材库管理系统

#### 介绍
WML可用于收集、整理、共享项目中的“图片、视频、音频、文档”等各种素材，让工作变得更有效率！WML为BS架构，素材统一存放于服务器中，用户通过浏览器管理和使用素材库中的素材。
#### 最新版本v0.1.0
- 后台-分类管理/标签管理
- 后台-人员机构岗位管理/参数设置
- 前台-布局（三段布局，左文件分类、中文件列表、右文件信息）
- 前台-文件批量上传
- 前台-资源列表（列数，布局，刷新，排序，检索）
- 前台-资源详情（预览，图片取色，图片放大、流媒体播放）
- 前台-文件管理（删除、移动分类、多选删除、多选移动、下载、部分格式预览【pdf、图片、mp4、mp3】）
- 前台-社交功能（访问量、评价、评论、访问记录、收藏、订阅）

#### 操作界面
![输入图片说明](doc/ui%E5%9B%BE%E7%89%87%E5%88%97%E8%A1%A8.png)
![输入图片说明](doc/imgs%E5%9B%BE%E7%89%87%E5%88%97%E8%A1%A82.png)
![输入图片说明](doc/imgs%E5%9B%BE%E7%89%87%E5%88%97%E8%A1%A83.png)
![输入图片说明](doc/imgs%E5%A4%9A%E9%80%89%E6%93%8D%E4%BD%9C2.png)
![输入图片说明](doc/imgs%E8%A7%86%E9%A2%91%E5%88%97%E8%A1%A82.png)
![输入图片说明](doc/imgspdf%E5%88%97%E8%A1%A82.png)

#### 软件架构

    
- jdk8
- maven
- spring4
- spring-mvc4
- hibernate4
- bootstrap
- tomcat8
- mysql5.7




#### 安装教程

安装包下载：[http://www.wcpdoc.com/webspecial/home/Pub2c94830b877dfa330187bc465e744b15.html](http://www.wcpdoc.com/webspecial/home/Pub2c94830b877dfa330187bc465e744b15.html)

视频教程：[http://course.wcpknow.com/classweb/Pubview.do?classid=2c9180838722e1ec0187c20a2b8f0e38](http://course.wcpknow.com/classweb/Pubview.do?classid=2c9180838722e1ec0187c20a2b8f0e38)

#### 使用说明



- maven部署源码（主模块：WLP/src/wlp-web ）编译顺序：wml-core > wml-parameter > wml-report > wml-authority > wml-quartz > wml-file > wml-material>wml-tag >wml-web
- 创建数据库，数据库脚本在 doc/目录下
- 修改数据库配置文件 wml-web\src\main\resources\jdbc.properties
- 项目编译后可直接部署于tomcat8，mysql5.7中运行，支持jdk8




## 开源项目推荐
	
> WCP:知识管理系统 [https://gitee.com/macplus/WCP](https://gitee.com/macplus/WCP)

> WDA:文件转换组件（附件在线预览）[https://gitee.com/macplus/WDA](https://gitee.com/macplus/WDA)

> WTS:在线答题系统 [https://gitee.com/macplus/WTS](https://gitee.com/macplus/WTS)

> WLP:在线学习系统 [https://gitee.com/macplus/WLP](https://gitee.com/macplus/WLP)

> PLOGS:项目任务日志管理系统 [https://gitee.com/macplus/plogs](https://gitee.com/macplus/plogs)

> WSCH:鉴权内容检索系统 [https://gitee.com/macplus/wsch](https://gitee.com/macplus/wsch)

## 商业版产品介绍

> 知识库/在线答題/在线学习产品介绍 [http://www.wcpknow.com/home/index.html](http://www.wcpknow.com/home/index.html)
